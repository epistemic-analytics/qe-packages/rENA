library(data.table)

real_data <- openxlsx::read.xlsx("~/Downloads/20181129_M_01_Coded_Standard_Affect_FINAL.xlsx")
real_data <- data.table::as.data.table(real_data)
real_data_inflated <- real_data[, { rbind(.SD, .SD, .SD) } ,by = seq(nrow(real_data))]
real_data_inflated$PositiveAffect = real_data[, as.matrix(.SD), by = seq(nrow(real_data)), .SDcols = c("PosAffect_KEN_1", "PosAffect_USA_1", "PosAffect_USA_2")]$V1
real_data_inflated$RowID <- seq(nrow(real_data_inflated))
real_data_inflated$discourse <- real_data_inflated$seq

KEN_1 = "Ateamate"
USA_1 = "Meghan"
USA_2 = "Kallyn"

users <- c("PosAffect_KEN_1", "PosAffect_USA_1", "PosAffect_USA_2")
user_codes <- paste0("PosAffect_", users)

create_sample <- function(
  size = 2000,
  # speaker_count = 4,
  conversation_count = 4
) {
  data.table(
    row_id = seq(size),
    discourse = rep(1:(size / speaker_count), each = speaker_count),
    speaker = LETTERS[sample(1:speaker_count, size, replace = TRUE)],
    day  = rep(seq(conversation_count), each = (size / conversation_count)),
    emotion_one = sample(0:1, size = size, replace = TRUE),
    emotion_two = sample(0:1, size = size, replace = TRUE),
    emotion_three = sample(0:1, size = size, replace = TRUE),
    emotion_four = sample(0:1, size = size, replace = TRUE),
    text_one = rep(sample(0:1, (size /   speaker_count), replace = TRUE), each = speaker_count),
    text_two = rep(sample(0:1, (size /   speaker_count), replace = TRUE), each = speaker_count),
    text_three = rep(sample(0:1, (size / speaker_count), replace = TRUE), each = speaker_count),
    text_four = rep(sample(0:1, (size /  speaker_count), replace = TRUE), each = speaker_count)
  )
}

generate_set <- function(
  x,
  conversation,
  emotion_codes,
  text_codes
) {
  full_adj <- rENA:::namesToAdjacencyKey(c(emotion_codes, text_codes))
  full_adj_names <- apply(full_adj, 2, paste, collapse = ".")
  # browser()
  emotion_window_size = 2
  text_window_size = 6

  newset = list()
  class(newset) <- c("ena.set", class(newset))

  newset$model$raw.input <- data.table::copy(x)
  newset$model$row.connection.counts <- newset$model$raw.input[ , (full_adj_names) := {
    to_ret <- sapply(seq(.N), function(i) {
      this_id <- .SD[i, RowID]
      discourse_id <- .SD[i, discourse]

      emotion_stanza <- discourse_id:(discourse_id - (emotion_window_size - 1))
      emotion_stanza <- emotion_stanza[emotion_stanza > 0]
      emotion_stanza_codes <- .SD[discourse %in% emotion_stanza, c("RowID", emotion_codes), with = F]
      emotion_stanza_refs_codes <- emotion_stanza_codes[RowID != this_id, ]

      text_stanza <- discourse_id:(discourse_id - (text_window_size - 1))
      text_stanza <- text_stanza[text_stanza > 0]
      text_stanza_codes <- .SD[discourse %in% text_stanza, c("RowID", text_codes), with = F][seq(1,.N,2),]
      text_stanza_refs_codes <- text_stanza_codes[RowID != this_id, ]

      all_cooccurs <- vector_to_ut(
        as.matrix(c(colSums(emotion_stanza_codes[, -1]), colSums(text_stanza_codes[, -1])))
      )
      refs_cooccurs <- vector_to_ut(
        as.matrix(c(colSums(emotion_stanza_refs_codes[, -1]), colSums(text_stanza_refs_codes[, -1])))
      )
      cooccurs <- all_cooccurs - refs_cooccurs

      as.numeric(cooccurs)
    })

    returning <- split(t(to_ret), col(t(to_ret)))

    returning
  }, by = c(conversation)][]
  class(newset$model$row.connection.counts) <- c("row.connections", class(newset$model$row.connection.counts))

  newset$connection.counts <- newset$model$row.connection.counts[, lapply(.SD, sum), by = c("Speaker"), .SDcols = full_adj_names]
  for (i in seq(ncol(newset$connection.counts))) {
    set(newset$connection.counts, j = i,
        value = rENA:::as.ena.co.occurrence(newset$connection.counts[[i]]))
  }
  set(newset$connection.counts, j = 1L, value = rENA:::as.ena.metadata(newset$connection.counts[[1L]]))


  newset$rotation <- list(codes = c(emotion_codes, text_codes))
  newset$model$model.type <- "EndPoint"
  # browser()
  ena.make.set(newset)
}

set <- generate_set(
          real_data_inflated,
          conversation = "CollectionDate",
          emotion_codes = c("PositiveAffect"),
          text_codes = c("ContentFocus", "SocialDisposition",
              "Feedback", "InclusiveDisposition", "InfoSharing", "MediaProduction")
)
set %>%
  ena.plot(scale.to = "points") %>%
  ena.plot.network(colMeans(set$line.weights))
