// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>

using namespace Rcpp;
using namespace arma;


// [[Rcpp::export]]
List pca_c(arma::mat m, int dims = 2) {
  arma::mat pca;
  arma::mat score;
  arma::vec latent; // Eigen
  arma::vec tsquared;
  arma::princomp(pca, score, latent, tsquared, m);

  if(pca.n_cols > dims) {
    pca = pca.head_cols(dims);
  }

  return List::create(
    _["pca"] = pca,
    _["latent"] = latent
  );
}

// [[Rcpp::export]]
List svd_c(arma::mat m, int dims = 2) {
  arma::mat U;
  arma::vec s;
  arma::mat V;
  arma::svd(U, s, V, m);

  return List::create(
    _["U"] = U,
    _["s"] = s,
    _["V"] = V
  );
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically
// run after the compilation.
//

/*** R
pca_c(a, 5)
svd_c(a)
*/
