codeNames = c('Data','Technical.Constraints','Performance.Parameters',
  'Client.and.Consultant.Requests','Design.Reasoning','Collaboration');

accum = ena.accumulate.data(
  units = RS.data[,c("UserName","Condition")],
  conversation = RS.data[,c("GroupName","ActivityNumber")],
  metadata = RS.data[,c("CONFIDENCE.Change","CONFIDENCE.Pre","CONFIDENCE.Post","C.Change")],
  codes = RS.data[,codeNames],
  window.size.back = 4,
  window = "C",
  model = "A"
);
set = ena.make.set(accum);

### get mean network plots
first.game.lineweights = as.matrix(set$line.weights$Condition$FirstGame)
first.game.mean = colMeans(first.game.lineweights)

second.game.lineweights = as.matrix(set$line.weights$Condition$SecondGame)
second.game.mean = colMeans(second.game.lineweights)

subtracted.network = first.game.mean - second.game.mean

# Plot dimension 1 against ActivityNumber metadata
dim.by.activity = cbind(
    as.matrix(set$points)[,1],
    set$trajectories$ActivityNumber * .8/14-.4  #scale down to dimension 1
)

ena.plot(set) %>% ena.plot.points(as.matrix(set$points[, .SD[nrow(.SD)], by = ENA_UNIT])[,1:2])

ena.plot(set) %>% ena.plot.trajectory(
  points = dim.by.activity,
  names = unique(accum$trajectories$ENA_UNIT),
  by = set$trajectories$ENA_UNIT,
  labels = ""
)
