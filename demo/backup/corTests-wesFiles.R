library('rPython')
devtools::load_all(".")
python.load( system.file("demo","backup","points-v3-1.py", package = "rENA") )

folder = "/Users/clmarquart/Downloads/forCody/"

sets = c("landScience-users-in-groups-strophe", "newcomb", "rescushell-newer-codes", "rescushell-users-in-groups-ws4")


interpretive.correlation = function(x, t, w) {
  # :::::: parameter validation ::::::
  if (class(x) != "matrix" & class(x) != 'data.frame')
    x = matrix(x, ncol = 1, dimnames = list(names(x), "PC1"))

  MPS = function(xi){
    CC = which(upper.tri(diag(length(xi))),arr.ind=T)
    out = (xi[CC[,1]] + xi[CC[,2]])/2
    return(out)
  }

  N = nrow(x)
  K = nrow(t)
  M = min(c(ncol(x), ncol(t)))
  iK = which(upper.tri(diag(K)), arr.ind = T)

  mps = matrix(nrow = choose(N, 2), ncol = M)
  for (m in 1:M) mps[, m] = MPS(x[, m])

  dpps = matrix(nrow=choose(K, 2), ncol=M)
  for (m in 1:M) dpps[, m] = t[iK[, 1], m] - t[iK[, 2], m]

  centroids = matrix(nrow = K, ncol = M)
  for (m in 1:M)
    centroids[, m] = apply(w, 1, function(z) sum(z*mps[, m])/sum(z))

  dcen = matrix(nrow = choose(K, 2), ncol = M)
  for (m in 1:M) dcen[, m] = centroids[iK[, 1], m] - centroids[iK[, 2], m]

  icorr = sapply(1:M, function(m) cor(dpps[, m], dcen[, m]))

  return(icorr)
}

sets.list = list()
set.index = 1;
for(set in sets) {
  set.files = list.files(folder, pattern = set)
  sets.list[[set.index]] = list(
    name = set,
    nodes = read.csv( list.files(folder, pattern = paste0(set, "_x.csv"), full.names = T) ),
    rotated.data = read.csv( list.files(folder, pattern = paste0(set, "_t.csv"), full.names = T) ),
    normed.weights = read.csv( list.files(folder, pattern = paste0(set, "_p.csv"), full.names = T) )
  )
  set.index = set.index + 1;
}

res.list = list()
for(set in sets.list) {
  print(paste("Working on set: ", set$name))

  data = make.adj.matrices(set$normed.weights)
  rot = set$rotated.data
  rot = as.matrix(rot)
  colnames(rot) = NULL
  pyt = python.call("get_positions", data, rot);
  pyt$nodes = t(matrix(unlist(pyt$nodes), nrow=2));

  res.list[[set$name]] = list()
  res.list[[set$name]]$jeff = pyt$correlations[[1]]

  cpp = linderoth_pos_es(as.matrix(set$normed.weights), rot)
  res.list[[set$name]]$cody = cpp$correlations

  res.list[[set$name]]$wes = interpretive.correlation(set$nodes, set$rotated.data, set$normed.weights)
}
