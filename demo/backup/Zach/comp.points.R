### function for correlating points on ENA dimensions

comp.points = function(set1,set2,dim){

  # browser()

  points.1 = data.frame(set1$points)
  points.2 = data.frame(set2$points)

  points.1 = points.1[,dim]
  points.2 = points.2[,dim]

  point.cor = cor.test(points.1,points.2)

  res = list(correlation = point.cor$estimate, lower.ci = point.cor$conf.int[1], upper.ci = point.cor$conf.int[2])

  return(res)

}
