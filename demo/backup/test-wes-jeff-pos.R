library(data.table)

N = length(res.list)
new.results = data.table(
  "name"=character(N),
  "wes.1"=numeric(N),"wes.2"=numeric(N),
  # "wes.1.int"=numeric(N),"wes.2.int"=numeric(N),€
  "jeff.1"=numeric(N),"jeff.2"=numeric(N),
  "cody.1"=numeric(N),"cody.2"=numeric(N),
  "cody.passes" = logical(N),
  "jeff.passes" = logical(N),
  "units"=character(N), "conversations"=character(N), "codes"=character(N)
);

result.row = 1;
for(name in names(res.list)) {
  new.results[result.row]$name = name
  new.results[result.row]$wes.1 = res.list[[name]]$wes[1]
  new.results[result.row]$wes.2 = res.list[[name]]$wes[2]
  new.results[result.row]$jeff.1 = res.list[[name]]$jeff[1]
  new.results[result.row]$jeff.2 = res.list[[name]]$jeff[2]
  new.results[result.row]$cody.1 = res.list[[name]]$cody[1]
  new.results[result.row]$cody.2 = res.list[[name]]$cody[2]
  new.results[result.row]$cody.passes <- all.equal(res.list[[name]]$cody, res.list[[name]]$jeff, tolerance = 1.5e-3) == T

  new.results[result.row]$jeff.passes <- all.equal(res.list[[name]]$jeff, res.list[[name]]$wes, tolerance = 1.5e-3) == T
  result.row = result.row + 1;
}
