

adj.names = c("A.s","A.r","B.s","B.r","C.s","C.r")
adj.vecs = t(matrix(c(
    c(2,0,0,2,0,0),
    c(0,1,1,0,0,0),
    c(0,1,0,0,1,0),
    c(0,0,0,1,1,0)
  )
  , nrow=6, dimnames = list(adj.names)
))
adj.vecs[adj.vecs > 1] = 1
adj.vecs.sum = colSums(adj.vecs)
adj.vec.refs = colSums(adj.vecs[1:3,])
adj.vec.ref = adj.vecs[4,]

adj.vec.mat = adj.vecs.sum %*% t(adj.vecs.sum)
rownames(adj.vec.mat) = adj.names


adj.mask = matrix(1, nrow=nrow(adj.vec.mat), ncol=ncol(adj.vec.mat))
diag(adj.mask) = 0

colnames(adj.mask)= colnames(adj.vec.mat)
rownames(adj.mask)= rownames(adj.vec.mat)
adj.mask[grep("\\.r", rownames(adj.vec.mat)),grep("\\.r", colnames(adj.vec.mat))] = 0
adj.mask[grep("\\.s", rownames(adj.vec.mat)),grep("\\.s", colnames(adj.vec.mat))] = 0
adj.mask["A.s","A.r"] = 0
adj.mask["B.s","B.r"] = 0
adj.mask["C.s","C.r"] = 0
adj.mask["A.r","A.s"] = 0
adj.mask["B.r","B.s"] = 0
adj.mask["C.r","C.s"] = 0

adj.vec.masked = adj.vec.mat * adj.mask

mat.ut = function(mat) {
  matrix(
    mat[upper.tri(mat)],
    nrow=1,
    dimnames=list(NULL, svector_to_ut(colnames(mat)))
  )
}
adj.vec.masked.ut = mat.ut(adj.vec.masked)

adj.vec.refs.masked = adj.vec.refs * adj.mask
adj.vec.refs.masked.ut = mat.ut(adj.vec.refs.masked)
