// !preview r2d3 data = jsonlite::toJSON(pts), viewer='browser', d3_version = 6

console = window.console;
window.data = data;

window.width = width;
window.height = height;

const
  point_max_x = window.point_max_x = data.reduce( (x, d) => Math.max(Math.abs(d.MR1), x), 0),
  point_max_y = window.point_max_y = data.reduce( (x, d) => Math.max(Math.abs(d.SVD2), x), 0),
  origin_x = window.origin_x = width / 2,
  origin_y = window.origin_y = height / 2,
  multiplier_x = window.multiplier_x = (width / 2) * point_max_x,
  multiplier_y = window.multiplier_y = (height / 2) * point_max_y
;
console.log(point_max_x, point_max_y);


svg.append("rect")
  .attr('x', 0).attr('y', 0)
  .attr('width', width)
  .attr('height', height)
  .attr('fill', '#FFFFFF')
;
svg.append("line")
  .attr('x1', width / 2).attr('x2', width / 2)
  .attr('y1', 0).attr('y2', height)
  .attr('stroke-width', 1)
  .attr('stroke', '#000')
;
svg.append("line")
  .attr('y1', height / 2).attr('y2', height / 2)
  .attr('x1', 0).attr('x2', width)
  .attr('stroke-width', 1)
  .attr('stroke', '#000')
;

window.console.dir(data)
let
  position_point = (d, wh) => {
    let pt_ = wh === "x" ? d['MR1'] : d['SVD2'];
    let v = ((pt_ *  this['multiplier_'+wh]) + this['origin_'+wh]);
    return(v);
  }
;

svg.selectAll('circle')
  .data(data, function(x) { return x.id })
  .join(
    enter => {
      return enter.append("svg:circle")
        .attr('cx', (d) => position_point(d, 'x'))
        .attr('cy', (d) => position_point(d, 'y'))
        .attr('fill', (d) => d.color)
        .attr('r', 5)
      ;
    },
    update => {
      return update
        .attr('cx', function(d) { let v = ((d.MR1 + originX) * 1); return(v); })
        .attr('cy', function(d) { let v = ((d.SVD2 + originY) * 1); return(v); })
      ;
    },
    exit => exit
  )
;
