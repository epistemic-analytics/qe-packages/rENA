import time
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import json

from numpy import linalg as LA

def compute_difference_correlation(c,t):
    K=c.shape[0]
    M=c.shape[1]
    correlations = []

    for i in range(M):
        dlen = 0
        clen = 0
        numerator = 0
        for a in range(K):
            for b in range(K):
                dabi=t[a,i] - t[b,i]
                cabi=c[a,i] - c[b,i]
                numerator += dabi*cabi
                dlen += (dabi*dabi)
                clen += (cabi*cabi)
        corr=numerator/(math.sqrt(dlen)*math.sqrt(clen))
        correlations.append(corr)

    return correlations

def component_norm(w, t, x):
    K = w.shape[0]
    N = w.shape[1]

    norm = 0
    for k in range(K):
        ck = np.dot(w[k,:],x)
        tk = t[k]
        norm += (ck-tk)*(ck-tk)
    return(math.sqrt(norm))

def get_positions(p, t):
    p = np.array(p);
    t = np.matrix(t);

    K = p.shape[0]
    N = p.shape[1]
    M=t.shape[1]

    # Now we start
    w = np.zeros((K,N))
    for k in range(K):
        for i in range(N):
            for j in range(N):
                if (i <= j):
                    w[k,i] += 0.5*p[k,i,j]
                else:
                    w[k,i] += 0.5*p[k,j,i]

    # Need to normalize weights so that the X looks OK
    for k in range(K):
        length=0
        for i in range(N):
            length += abs(w[k,i])
        if length < 0.00001:
            length = 0.00001
        for i in range(N):
            w[k,i] = w[k,i]/length


    # Note that Sigma doesn't depend on M.  Need only compute it once
    Sigma = np.zeros((N,N))
    for a in range(K):
        for b in range(K):
            q = w[a,:] - w[b,:]
            Sigma = np.add(Sigma, np.outer(q,q))

    # It may not be true that sigma is stricly positive definite, in
    #  which case you better do something
    eigval, eigven = LA.eig(Sigma)

    mineigval = np.amin(eigval)
    if mineigval < 0.001:
        print("Warning: Sigma not positive definite.  Eigenvalue: " + str(mineigval))
        print("   Adding 0.1 to diagonal")
        Sigma = Sigma + 0.1*np.identity(N)

    #  Inverses make me sad.  But N is so small it should be OK
    #  If things ge weird later, should probably do Cholesky
    Sinv = LA.inv(Sigma)

    # Let's do it component by component.
    #  It's not really necessary, but maybe it is faster
    X = np.zeros((M,N))
    for i in range(M):
        Delta = np.zeros((K,K))
        for a in range(K):
            for b in range(K):
                Delta[a,b] = t[a,i] - t[b,i];

        R = LA.norm(Delta, 'fro')

        gamma = np.zeros(N)
        for j in range(N):
            for a in range(K):
                for b in range(K):
                    gamma[j] += Delta[a,b]*(w[a,j]-w[b,j])


        x = np.matmul(Sinv, gamma)
        alpha = R/math.sqrt(np.dot(gamma,x))
        x = alpha*x

        dist=component_norm(w,t[:,i],x)

        # Now scale each component independently
        #  Solve a little 2 by 2
        sumck = 0
        sumtk = 0
        sumcksq = 0
        sumcktk = 0

        for k in range(K):
            ck = np.dot(w[k,:],x)
            tk = t[k,i]
            sumck += ck
            sumcksq += (ck*ck)
            sumtk += tk
            sumcktk += (ck*tk)

        det = K*sumcksq - (sumck*sumck)

        alpha = (1/det) * (K*sumcktk - sumck*sumtk)
        beta = (1/det) * (-sumck*sumcktk + sumtk*sumcksq)

        x = alpha*x + beta

        X[i,:] = x

    # Compute centroids for plotting
    c = np.matmul(X,w.transpose()).transpose()
    correlations = compute_difference_correlation(c,t)
    ret = {}
    ret["correlations"] = correlations,
    ret["centroids"] = c.tolist(),
    ret["weights"] = w.tolist()
    ret["nodes"] = X.T.tolist(),
    ret["points"] = t.tolist()
    return(ret)
